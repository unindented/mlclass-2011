function [J, grad] = linearRegCostFunction(X, y, theta, lambda)
%LINEARREGCOSTFUNCTION Compute cost and gradient for regularized linear
% regression with multiple variables
%   [J, grad] = LINEARREGCOSTFUNCTION(X, y, theta, lambda) computes the cost of
%   using theta as the parameter for linear regression to fit the data points in
%   X and y. Returns the cost in J and the gradient in grad.

% Initialize some useful values
[m, n] = size(X);

% =============================== YOUR CODE HERE ===============================
% Instructions: Compute the cost and gradient of regularized linear regression
%               for a particular choice of theta.
%
%               You should set J to the cost and grad to the gradient.

predictions = X * theta;
errors = predictions - y;

regtheta = [zeros(1, size(theta, 2)) ; theta(2:n)];

% Cost
J =  (1 / (2 * m)) * (errors' * errors);
% Regularized cost
J += (lambda / (2 * m)) * (regtheta' * regtheta);

% Gradients
grad = (1 / m) * (errors' * X)';
% Regularized gradients
grad += (lambda / m) * regtheta;

% ==============================================================================

grad = grad(:);

end
