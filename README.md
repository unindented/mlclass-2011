# Machine Learning Class 2011

Exercises for the [Machine Learning Class](http://www.ml-class.org/) taught by [Professor Andrew Ng](http://ai.stanford.edu/~ang/) between October and December 2011.

## Meta

* Code: `git clone git@bitbucket.org:unindented/mlclass-2011.git`
* Home: <https://bitbucket.org/unindented/mlclass-2011>

## Contributors

Daniel Perez Alvarez ([unindented@gmail.com](mailto:unindented@gmail.com))

## License

### Exercises

Copyright (c) 2011 [Professor Andrew Ng](http://ai.stanford.edu/~ang/).

### Solutions

Copyright (c) 2011 [Daniel Perez Alvarez](http://unindented.org/).
